from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.floatlayout import FloatLayout
from kivy.properties import BooleanProperty
from kivy.clock import Clock
from datetime import datetime, timedelta

def GetTime(seconds):
	sec = timedelta(seconds=seconds)
	return datetime(1,1,1) + sec


filename = "TimeWorked.txt"

class Counter(FloatLayout):
	counting = BooleanProperty(False)
	timePassed = 0
	startingTime = 0
	fileTime = ""

	def loadTime(self):
		with open(filename) as stream:
			self.fileTime = stream.read()
			print("Time from file: " + self.fileTime)
			self.startingTime = float(self.fileTime)
			self.timePassed = self.startingTime

	def startCounter(self):
		if(self.counting != False):
			self.loadTime()
			timeHMS = GetTime(self.timePassed)
			self.ids.timer.text = str(timeHMS.hour) + " : " + str(timeHMS.minute) + " : " + str(timeHMS.second)
			self.counting = True

	def saveTime(self):
		with open(filename, 'w') as stream:
           		stream.write(str(self.timePassed))
		

	def stopCounter(self):
		self.saveTime()
		self.counting = False
		print("Time Saved")

	def update(self, dt):
		timeHMS = GetTime(self.timePassed)
		self.ids.timer.text = str(timeHMS.hour) + " : " + str(timeHMS.minute) + " : " + str(timeHMS.second)
		if(self.counting == True):
			self.timePassed += float(dt)
		
		# if(isDigit(self.fileTime)):
		# 	print(self.fileTime)

		if(self.timePassed - self.startingTime > 300):
			self.saveTime()
			self.startingTime = self.timePassed
			print("Periodic Time Save")

	def on_stop(self):
		self.stopCounter()

class CounterApp(App):
	def build(self):
		counter = Counter()
		Clock.schedule_interval(counter.update, 1)
		return counter


if __name__=='__main__':
	CounterApp().run()